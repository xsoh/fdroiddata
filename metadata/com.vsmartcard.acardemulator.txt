Categories:Development
License:GPLv3
Web Site:https://frankmorgner.github.io/vsmartcard/ACardEmulator/README.html
Source Code:https://github.com/frankmorgner/vsmartcard
Issue Tracker:https://github.com/frankmorgner/vsmartcard/issues

Auto Name:Smart Card Emulator
Summary:Use your phone as contact-less smart card
Description:
The Android Smart Card Emulator allows the emulation of a contact-less smart
card. The emulator uses Android's HCE to fetch APDUs from a contact-less reader
and delegate them to Java Card Applets. The app includes the Java Card
simulation runtime of jCardSim as well as the following Java Card applets:

* Hello World applet
* OpenPGP applet
* OATH applet
* ISO applet
.

Repo Type:git
Repo:https://github.com/frankmorgner/vsmartcard.git

Build:1.0,1
    commit=e52a9a5bbc59725af720106983ffea48e6b01407
    subdir=ACardEmulator/app
    submodules=yes
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

