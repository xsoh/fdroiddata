Categories:Games,Internet
License:GPLv3,CC-BY-SA 3.0
Web Site:http://freeminer.org
Source Code:https://github.com/freeminer/freeminer
Issue Tracker:https://github.com/freeminer/freeminer/issues

Name:freeminer
Summary:Minecraft-inspired sandbox game
Description:
Fork of [[net.minetest.minetest]], a block sandbox game and a game engine
inspired by InfiniMiner, Minecraft and the like.

Features:

* Explore, dig and build in a voxel world, and craft stuff from raw materials to help you along the way.
* Play with your friends on public servers or self hosted servers
* Easy plugin based Modding API used to add blocks, tools and features to the game.
* Voxel based lighting with gameplay consequences (light caves and buildings with torches)
* Almost infinite world and several beautiful map generators.
* Runs natively on Windows, Linux, OS X, FreeBSD and Android
* Supports multiple languages, translated by the community.
* A constant development to add new functionalities for end-users
.

Repo Type:git
Repo:https://github.com/freeminer/freeminer.git

Build:0.4.12.6.13,13
    commit=202a17e58eecb6325a55bee5404e1d4a59021047
    subdir=build/android
    submodules=yes
    output=bin/freeminer-release-unsigned.apk
    build=printf "%s\n%s\n%s" "ANDROID_NDK = $$NDK$$" "NDK_MODULE_PATH = $$NDK$$/toolchains" "SDKFOLDER = $$SDK$$" > path.cfg && \
        for n in 1 2 3; do make release && \
        break; done
    buildjni=no

Build:0.4.12.6.15,15
    commit=65dd040a3ef160e8967e96b2c6abf7c772859d30
    subdir=build/android
    submodules=yes
    output=bin/freeminer-release-unsigned.apk
    build=printf "%s\n%s\n%s" "ANDROID_NDK = $$NDK$$" "NDK_MODULE_PATH = $$NDK$$/toolchains" "SDKFOLDER = $$SDK$$" > path.cfg && \
        for n in 1 2 3; do make release && \
        break; done
    buildjni=no

Build:0.4.12.6.25,25
    disable=build fails
    commit=70af3286ae50eb640463c32bb0a9cd5941bf6e94
    subdir=build/android
    submodules=yes
    output=bin/freeminer-release-unsigned.apk
    build=printf "%s\n%s\n%s" "ANDROID_NDK = $$NDK$$" "NDK_MODULE_PATH = $$NDK$$/toolchains" "SDKFOLDER = $$SDK$$" > path.cfg && \
        for n in 1 2 3; do make release && \
        break; done
    buildjni=no

Auto Update Mode:None
Update Check Mode:None
Current Version:0.4.12.6.25
Current Version Code:25

