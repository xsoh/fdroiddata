Categories:Connectivity
License:MPL2
Web Site:https://www.libreoffice.org
Source Code:https://gerrit.libreoffice.org/gitweb?p=impress_remote.git;a=summary
Issue Tracker:https://www.libreoffice.org/bugzilla/buglist.cgi?product=LibreOffice&component=Android%20Impress%20Remote
Donate:https://donate.libreoffice.org
FlattrID:256305
Bitcoin:129jj3HiLfj3zCfqoro3sMTdovizXEdo8A

Auto Name:Impress Remote
Summary:Remote for presentations
Description:
Interact with your slideshow presentation from your Android device.

Features:

* Slide previews
* Speaker notes
* Play/Pause presentation
* Set timers

To set up the app with your computer, follow this guide:
[https://wiki.documentfoundation.org/Development/Impress/RemoteHowTo
RemoteHowTo]

We cannot build the latest versions because they depend on the Google Play
Services libraries to build.
.

Repo Type:git
Repo:git://gerrit.libreoffice.org/impress_remote

Build:2.0.0,9
    commit=sdremote-2.0.0
    subdir=android/sdremote
    srclibs=1:Support-v7@android-sdk-4.4.2_r1
    extlibs=android/android-support-v4.jar
    prebuild=mv libs/android-support-v4.jar $$Support-v7$$/libs/

Build:2.1.1,11
    commit=sdremote-2.1.1
    subdir=android/sdremote
    gradle=yes

Build:2.1.3,13
    commit=sdremote-2.1.3
    subdir=android/sdremote/mobile
    gradle=yes
    rm=ios

Build:2.2.1,15
    disable=google play services
    commit=sdremote-2.2.1
    subdir=android/sdremote/mobile
    gradle=yes

Maintainer Notes:
Upstream uses a weird package name and version file layout. The information is
in the parent directory. We don't evaluate gradle files, so it doesn't work.
.

Auto Update Mode:None
#Update Check Mode:Tags ^sdremote-
#Update Check Name:Ignore
Update Check Mode:None
Current Version:2.2.1
Current Version Code:15

