Categories:Internet
License:MIT
Web Site:https://github.com/tobykurien/WebApps/blob/HEAD/README.md
Source Code:https://github.com/tobykurien/webapps
Issue Tracker:https://github.com/tobykurien/webapps/issues

Auto Name:WebApps
Summary:Sandbox for webapps
Description:
Provide a secure way to browse popular webapps by eliminating referrers, 3rd
party requests, cookies, cross-site scripting, etc.

It accomplishes this by providing a sandbox for multiple webapps (like Google's
apps, Facebook, Twitter, etc.). Each webapp will run in it's own sandbox, with
3rd party requests (images, scripts, iframes, etc.) blocked, and all external
links opening in an external default web browser (which should have cookies,
plugins, flash, etc. disabled).

By default, all HTTP requests are blocked (only HTTPS allowed). This improves
security, especially on untrusted networks. The app can also handle HTTPS links
and open them in their own sandbox.

Based on [[com.tobykurien.google_news]].
.

Repo Type:git
Repo:https://github.com/tobykurien/webapps

Build:v1.3,3
    commit=b58dcc3d907898ff4c5c3e00db474a3b96d66223
    gradle=yes
    rm=libs/*.jar

Build:v1.4,4
    commit=19bd52c20cc33183cfe3a683266a43bd49ec8d78
    gradle=yes
    rm=libs/*.jar

Build:v2.0,5
    disable=foo
    commit=94163a8a7bf1f379b6230922a7d1f4a90b611f99
    gradle=yes

Build:v2.1,6
    disable=builds, but jar file
    commit=v2.1
    gradle=yes
    rm=gradle-libs/*

Auto Update Mode:None
#Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:v2.2
Current Version Code:7

